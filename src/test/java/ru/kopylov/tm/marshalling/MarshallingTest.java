package ru.kopylov.tm.marshalling;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kopylov.tm.Application;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.entity.User;

import java.io.IOException;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class MarshallingTest {

    private final ObjectMapper objectMapperJson = new MappingJackson2HttpMessageConverter().getObjectMapper();

    private final ObjectMapper objectMapperXml = new MappingJackson2XmlHttpMessageConverter().getObjectMapper();

    @Test
    public void projectJson() throws IOException {
        final Project project = new Project();
        project.setName("TestName");
        project.setDescription("TestDescription");
        project.setDateStart(new Date());
        project.setDateFinish(new Date());
        final String json = objectMapperJson.writeValueAsString(project);
        final Project projectJson = objectMapperJson.readValue(json, Project.class);
        assertEquals(project.getId(), projectJson.getId());
        assertEquals(project.getName(), projectJson.getName());
        assertEquals(project.getDescription(), projectJson.getDescription());
        assertEquals(project.getDateStart(), projectJson.getDateStart());
        assertEquals(project.getDateFinish(), projectJson.getDateFinish());
        assertEquals(project.getState(), projectJson.getState());
    }

    @Test
    public void projectXml() throws IOException {
        final Project project = new Project();
        project.setName("TestName");
        project.setDescription("TestDescription");
        project.setDateStart(new Date());
        project.setDateFinish(new Date());
        final String xml = objectMapperXml.writeValueAsString(project);
        final Project projectJson = objectMapperXml.readValue(xml, Project.class);
        assertEquals(project.getId(), projectJson.getId());
        assertEquals(project.getName(), projectJson.getName());
        assertEquals(project.getDescription(), projectJson.getDescription());
        assertEquals(project.getDateStart(), projectJson.getDateStart());
        assertEquals(project.getDateFinish(), projectJson.getDateFinish());
        assertEquals(project.getState(), projectJson.getState());
    }

    @Test
    public void taskJson() throws IOException {
        final Task task = new Task();
        final Project project = new Project();
        task.setName("TestName");
        task.setDescription("TestDescription");
        task.setDateStart(new Date());
        task.setDateFinish(new Date());
        task.setProject(project);
        final String json = objectMapperJson.writeValueAsString(task);
        final Task taskJson = objectMapperJson.readValue(json, Task.class);
        assertEquals(task.getId(), taskJson.getId());
        assertEquals(task.getName(), taskJson.getName());
        assertEquals(task.getDescription(), taskJson.getDescription());
        assertEquals(task.getDateStart(), taskJson.getDateStart());
        assertEquals(task.getDateFinish(), taskJson.getDateFinish());
        assertEquals(task.getState(), taskJson.getState());
        assertEquals(task.getProject().getId(), taskJson.getProject().getId());
        assertEquals(task.getProject().getState(), taskJson.getProject().getState());
    }

    @Test
    public void taskXml() throws IOException {
        final Task task = new Task();
        final Project project = new Project();
        task.setName("TestName");
        task.setDescription("TestDescription");
        task.setDateStart(new Date());
        task.setDateFinish(new Date());
        task.setProject(project);
        final String xml = objectMapperXml.writeValueAsString(task);
        final Task taskJson = objectMapperXml.readValue(xml, Task.class);
        assertEquals(task.getId(), taskJson.getId());
        assertEquals(task.getName(), taskJson.getName());
        assertEquals(task.getDescription(), taskJson.getDescription());
        assertEquals(task.getDateStart(), taskJson.getDateStart());
        assertEquals(task.getDateFinish(), taskJson.getDateFinish());
        assertEquals(task.getState(), taskJson.getState());
        assertEquals(task.getProject().getId(), taskJson.getProject().getId());
        assertEquals(task.getProject().getState(), taskJson.getProject().getState());
    }

    @Test
    public void userJson() throws IOException {
        final User user = new User();
        user.setLogin("TestLogin");
        user.setPasswordHash("TestPassword");
        final String json = objectMapperJson.writeValueAsString(user);
        final User userJson = objectMapperJson.readValue(json, User.class);
        assertEquals(user.getId(), userJson.getId());
        assertEquals(user.getLogin(), userJson.getLogin());
        assertEquals(user.getPasswordHash(), userJson.getPasswordHash());
    }

    @Test
    public void userXml() throws IOException {
        final User user = new User();
        user.setLogin("TestLogin");
        user.setPasswordHash("TestPassword");
        final String xml = objectMapperXml.writeValueAsString(user);
        final User userJson = objectMapperXml.readValue(xml, User.class);
        assertEquals(user.getId(), userJson.getId());
        assertEquals(user.getLogin(), userJson.getLogin());
        assertEquals(user.getPasswordHash(), userJson.getPasswordHash());
    }

}
