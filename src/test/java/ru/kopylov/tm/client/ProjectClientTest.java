package ru.kopylov.tm.client;

import feign.FeignException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.User;

import java.util.List;

import static org.junit.Assert.*;

public class ProjectClientTest {

    private final String baseUrl = "http://localhost:8080/";

    private final String testId = "test-id9999";

    private final String testName = "TEST NAME";

    private String sessionId = "";

    private ProjectClient projectClient = ProjectClient.client(baseUrl);

    @Before
    public void setUp() throws Exception {
        final User user = new User();
        user.setLogin("test");
        user.setPasswordHash("test");
        sessionId = projectClient.login(user);
        final Project project = new Project();
        project.setId(testId);
        project.setName(testName);
        projectClient.addProject(sessionId, project);
    }

    @After
    public void tearDown() throws Exception {
        final Project project = projectClient.getProject(sessionId, testId);
        if (project != null) projectClient.deleteProject(sessionId, testId);
        projectClient.logout(sessionId);
    }

    @Test(expected = FeignException.class)
    public void wrongLogin() {
        final User user = new User();
        user.setLogin("nope");
        user.setPasswordHash("nope");
        projectClient.login(user);
    }

    @Test(expected = FeignException.class)
    public void getProjectsUnAuth() {
        final List<Project> projects = projectClient.getProjects(null);
    }

    @Test
    public void getProjects() {
        final List<Project> projects = projectClient.getProjects(sessionId);
        Assert.assertFalse(projects.isEmpty());
    }

    @Test
    public void getProjectsXml() {
        final List<Project> projects = projectClient.getProjectsXml(sessionId);
        Assert.assertFalse(projects.isEmpty());
    }

    @Test
    public void getProject() {
        final Project project = projectClient.getProject(sessionId, testId);
        Assert.assertNotNull(project);
        Assert.assertEquals(project.getName(), testName);
    }

    @Test
    public void updateProject() {
        final Project project = projectClient.getProject(sessionId, testId);
        String name = "testName";
        project.setName(name);
        projectClient.updateProject(sessionId, project);
        Assert.assertEquals(projectClient.getProject(sessionId, testId).getName(), name);
    }

    @Test
    public void deleteProject() {
        Assert.assertNotNull(projectClient.getProject(sessionId, testId));
        projectClient.deleteProject(sessionId, testId);
        Assert.assertNull(projectClient.getProject(sessionId, testId));
    }

    @Test
    public void addProject() {
        final String id = "test-id5555";
        final String name = "nameX";
        final Project project = new Project();
        project.setId(id);
        project.setName(name);
        Assert.assertEquals(projectClient.addProject(sessionId, project).getName(), name);
        Assert.assertEquals(projectClient.getProject(sessionId, id).getId(), id);
        projectClient.deleteProject(sessionId, id);
    }

}