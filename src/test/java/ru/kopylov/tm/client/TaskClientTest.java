package ru.kopylov.tm.client;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.entity.User;

import java.util.List;

import static org.junit.Assert.*;

public class TaskClientTest {

    private final String baseUrl = "http://localhost:8080/";

    private final String testId = "test-id8888";

    private final String testName = "TEST TASK";

    private String sessionId = "";

    private TaskClient taskClient = TaskClient.client(baseUrl);

    @Before
    public void setUp() throws Exception {
        final User user = new User();
        user.setLogin("test");
        user.setPasswordHash("test");
        sessionId = taskClient.login(user);
        final Task task = new Task();
        task.setId(testId);
        task.setName(testName);
        taskClient.addTask(sessionId, task);
    }

    @After
    public void tearDown() throws Exception {
        final Task task = taskClient.getTask(sessionId, testId);
        if (task != null) taskClient.deleteTask(sessionId, testId);
    }

    @Test
    public void getTasks() {
        final List<Task> tasks = taskClient.getTasks(sessionId);
        Assert.assertFalse(tasks.isEmpty());
    }

    @Test
    public void getTasksXml() {
        final List<Task> tasks = taskClient.getTasksXml(sessionId);
        Assert.assertFalse(tasks.isEmpty());
    }

    @Test
    public void getTask() {
        final Task task = taskClient.getTask(sessionId, testId);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), testName);
    }

    @Test
    public void updateTask() {
        final Task task = taskClient.getTask(sessionId, testId);
        String name = "testName";
        task.setName(name);
        taskClient.updateTask(sessionId, task);
        Assert.assertEquals(taskClient.getTask(sessionId, testId).getName(), name);
    }

    @Test
    public void deleteTask() {
        Assert.assertNotNull(taskClient.getTask(sessionId, testId));
        taskClient.deleteTask(sessionId, testId);
        Assert.assertNull(taskClient.getTask(sessionId, testId));
    }

    @Test
    public void addTask() {
        final String id = "test-id4444";
        final String name = "nameZ";
        final Task task = new Task();
        task.setId(id);
        task.setName(name);
        Assert.assertEquals(taskClient.addTask(sessionId, task).getName(), name);
        Assert.assertEquals(taskClient.getTask(sessionId, id).getId(), id);
        taskClient.deleteTask(sessionId, id);
    }

}