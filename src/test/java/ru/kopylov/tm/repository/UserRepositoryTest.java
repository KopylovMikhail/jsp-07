package ru.kopylov.tm.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.kopylov.tm.Application;
import ru.kopylov.tm.entity.User;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class UserRepositoryTest {
    
    @Autowired
    private UserRepository userRepository;

    @Test
    public void save() {
        final User user = new User();
        userRepository.save(user);
        assertNotNull(userRepository.findById(user.getId()));
        userRepository.deleteById(user.getId());
    }

    @Test
    public void findById() {
        final User user = new User();
        userRepository.save(user);
        assertEquals(userRepository.findById("nope"), Optional.empty());
        assertNotNull(userRepository.findById(user.getId()));
        userRepository.deleteById(user.getId());
    }

    @Test
    public void findByLogin() {
        final User user = new User();
        user.setLogin("TestLogin");
        userRepository.save(user);
        assertNull(userRepository.findByLogin("nope"));
        assertNotNull(userRepository.findByLogin(user.getLogin()));
        userRepository.deleteById(user.getId());
    }

    @Test
    public void deleteById() {
        final User user = new User();
        userRepository.save(user);
        assertNotNull(userRepository.findById(user.getId()));
        userRepository.deleteById(user.getId());
        assertEquals(userRepository.findById(user.getId()), Optional.empty());
    }

    @Test
    public void findAll() {
        final User user1 = new User();
        final User user2 = new User();
        userRepository.save(user1);
        userRepository.save(user2);
        assertTrue(userRepository.findAll().size() > 1);
        assertFalse(userRepository.findAll().isEmpty());
        userRepository.deleteById(user1.getId());
        userRepository.deleteById(user2.getId());
    }

}