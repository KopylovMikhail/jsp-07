package ru.kopylov.tm.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.kopylov.tm.Application;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.service.UserService;

import static org.hamcrest.Matchers.emptyString;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class UserControllerTest {

    @Autowired
    private UserService userService;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }

    @Test
    public void registration() throws Exception {
        mockMvc.perform(get("/registration"))
                .andExpect(status().isOk())
                .andExpect(view().name("registration"))
                .andExpect(forwardedUrl("/WEB-INF/views/registration.jsp"))
                .andExpect(model().attributeExists("username"))
                .andExpect(model().attributeExists("password"))
                .andExpect(model().attribute("username", emptyString()))
                .andExpect(model().attribute("password", emptyString()));
    }

    @Test
    public void registrationUser() throws Exception {
        final String username = "TEST_USER_0001";
        final String password = "TEST_PASS_0001";
        mockMvc.perform(post("/registration")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("username", username)
                .param("password", password)
        )
                .andExpect(status().is3xxRedirection());
        final User user = userService.findByLogin(username);
        assertNotNull(user);
        userService.deleteById(user.getId());
    }

    @Test
    public void login() throws Exception {
        mockMvc.perform(get("/login"))
                .andExpect(status().isOk())
                .andExpect(view().name("login"))
                .andExpect(forwardedUrl("/WEB-INF/views/login.jsp"));
    }

    @Test
    public void loginAction() throws Exception {
        final String username = "test";
        final String password = "test";
        mockMvc.perform(post("/loginAction")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("username", username)
                .param("password", password)
        )
                .andExpect(status().is3xxRedirection());
    }

}