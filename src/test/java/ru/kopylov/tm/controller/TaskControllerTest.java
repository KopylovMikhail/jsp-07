package ru.kopylov.tm.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import ru.kopylov.tm.Application;
import ru.kopylov.tm.api.service.IProjectService;
import ru.kopylov.tm.api.service.ITaskService;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.enumerated.State;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class TaskControllerTest {

    @Autowired
    ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void taskList() throws Exception {
        mockMvc.perform(get("/task_list"))
                .andExpect(status().isOk())
                .andExpect(view().name("tasks"))
                .andExpect(forwardedUrl("/WEB-INF/views/tasks.jsp"))
                .andExpect(model().attributeExists("tasks"))
                .andExpect(model().attribute("tasks", hasSize(taskService.findAll().size())));
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void viewTask() throws Exception {
        final Task task = new Task();
        task.setName("TEST_NAME");
        taskService.save(task);
        final String url = "/task_view/" + task.getId();
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(view().name("task_view"))
                .andExpect(forwardedUrl("/WEB-INF/views/task_view.jsp"))
                .andExpect(model().attributeExists("task"))
                .andExpect(model().attribute("task", hasProperty("id", is(task.getId()))))
                .andExpect(model().attribute("task", hasProperty("name", is(task.getName()))));
        taskService.deleteById(task.getId());
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void editTask() throws Exception {
        final Task task = new Task();
        task.setName("TEST_NAME");
        taskService.save(task);
        final String url = "/task_edit/" + task.getId();
        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(view().name("task_edit"))
                .andExpect(forwardedUrl("/WEB-INF/views/task_edit.jsp"))
                .andExpect(model().attributeExists("task"))
                .andExpect(model().attributeExists("projects"))
                .andExpect(model().attribute("projects", hasSize(projectService.findAll().size())))
                .andExpect(model().attribute("task", hasProperty("id", is(task.getId()))))
                .andExpect(model().attribute("task", hasProperty("name", is(task.getName()))));
        taskService.deleteById(task.getId());
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void saveTask() throws Exception {
        final Project project = new Project();
        final Task task = new Task();
        task.setName("TEST_0001");
        task.setState(State.PROCESS);
        taskService.save(task);
        projectService.save(project);
        final String url = "/task_edit/edit";
        mockMvc.perform(post(url)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("projectId", project.getId())
                .param("id", task.getId())
                .param("name", "TEST_0002")
                .param("description", task.getDescription())
                .param("dateStart", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(new Date()))
                .param("dateFinish", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(new Date()))
                .param("state", task.getState().name())
        )
                .andExpect(status().is3xxRedirection());
        ModelAndView modelAndView = mockMvc.perform(get(url)).andReturn().getModelAndView();
        final Task task2 = taskService.findById(task.getId());
        assertEquals(task.getId(), task2.getId());
        assertNotEquals(task.getName(), task2.getName());
        assertEquals("TEST_0002", task2.getName());
        assertNotNull(task2.getProject());
        taskService.deleteById(task.getId());
        projectService.deleteById(project.getId());
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void removeTask() throws Exception {
        final Task task = new Task();
        taskService.save(task);
        final String url = "/task_remove/" + task.getId();
        mockMvc.perform(get(url))
                .andExpect(status().is3xxRedirection());
        assertNull(taskService.findById(task.getId()));
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void createTask() throws Exception {
        final String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(new Date()).substring(0, 10);
        mockMvc.perform(get("/task_create"))
                .andExpect(status().isOk())
                .andExpect(view().name("task_create"))
                .andExpect(forwardedUrl("/WEB-INF/views/task_create.jsp"))
                .andExpect(model().attributeExists("dateStart"))
                .andExpect(model().attributeExists("dateFinish"))
                .andExpect(model().attribute("dateStart", containsString(date)))
                .andExpect(model().attribute("dateFinish", containsString(date)));
    }

    @Test
    @WithUserDetails(value = "test", userDetailsServiceBeanName = "userDetailsService")
    public void addProject() throws Exception {
        final String name = "TEST_0005";
        final String description = "DESCRIPTION_0005";
        final Date dateStart = new Date(5555L);
        final Date dateFinish = new Date(7777L);
        final String url = "/add_task";
        mockMvc.perform(post(url)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("name", name)
                .param("description", description)
                .param("dateStart", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(dateStart))
                .param("dateFinish", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(dateFinish))
                .param("state", State.PROCESS.name())
        )
                .andExpect(status().is3xxRedirection());
        Task task = null;
        List<Task> tasks = taskService.findAll();
        for (Task tsk : tasks) {
            if (name.equals(tsk.getName()) &&
                    description.equals(tsk.getDescription()) &&
                    dateStart.equals(tsk.getDateStart()) &&
                    dateFinish.equals(tsk.getDateFinish()) &&
                    State.PROCESS.equals(tsk.getState())) {
                task = tsk;
                break;
            }
        }
        assertNotNull(task);
        taskService.deleteById(task.getId());
    }

}