package ru.kopylov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kopylov.tm.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User findByLogin(String login);

}
