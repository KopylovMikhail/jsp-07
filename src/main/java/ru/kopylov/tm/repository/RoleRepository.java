package ru.kopylov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kopylov.tm.entity.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, String> {

}
