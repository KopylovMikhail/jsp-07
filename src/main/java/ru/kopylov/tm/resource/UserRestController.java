package ru.kopylov.tm.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.service.UserService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/rest")
public class UserRestController {

    @Autowired
    UserService userService;

    @Autowired
    private AuthenticationManager customAuthenticationManager;

    @PostMapping(value = "/login",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public String login(
            @RequestBody final User user,
            HttpServletRequest request
    ) {
        final String username = user.getLogin();
        final String password = user.getPasswordHash();
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        final Authentication authentication = customAuthenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
//        if (!authentication.isAuthenticated()) return null;
//        Enumeration<String> en = request.getHeaderNames();
//        while (en.hasMoreElements()) {
//            String hed = en.nextElement();
//            System.out.println(hed + ": " + request.getHeader(hed));
//        }
        return "JSESSIONID=" + request.getSession().getId();
    }

    @PostMapping(value = "/logout",
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

}
