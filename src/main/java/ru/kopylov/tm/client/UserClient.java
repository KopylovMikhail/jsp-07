package ru.kopylov.tm.client;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import ru.kopylov.tm.entity.User;

public interface UserClient {

    @PostMapping(value = "/rest/login", produces = MediaType.APPLICATION_JSON_VALUE)
    String login(User user);

    @PostMapping(value = "/rest/login", produces = MediaType.APPLICATION_XML_VALUE)
    String loginXml(User user);

    @PostMapping(value = "/rest/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    void logout(@RequestHeader("cookie") String sessionId);

    @PostMapping(value = "/rest/logout", produces = MediaType.APPLICATION_XML_VALUE)
    void logoutXml(@RequestHeader("cookie") String sessionId);

}
