package ru.kopylov.tm.client;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.kopylov.tm.entity.Project;

import java.util.List;

@FeignClient(value = "project")
public interface ProjectClient extends UserClient {

    static ProjectClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
//                .requestInterceptor(new BasicAuthRequestInterceptor("test", "test"))
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectClient.class, baseUrl);
    }

    @GetMapping(value = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    List<Project> getProjects(@RequestHeader("cookie") String sessionId);

    @GetMapping(value = "/projects", produces = MediaType.APPLICATION_XML_VALUE)
    List<Project> getProjectsXml(@RequestHeader("cookie") String sessionId);

    @GetMapping(value = "/project/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    Project getProject(@RequestHeader("cookie") String sessionId, @PathVariable("id") String id);

    @GetMapping(value = "/project/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    Project getProjectXml(@RequestHeader("cookie") String sessionId, @PathVariable("id") String id);

    @PutMapping(value = "/project", produces = MediaType.APPLICATION_JSON_VALUE)
    Project updateProject(@RequestHeader("cookie") String sessionId, Project project);

    @PutMapping(value = "/project", produces = MediaType.APPLICATION_XML_VALUE)
    Project updateProjectXml(@RequestHeader("cookie") String sessionId, Project project);

    @DeleteMapping(value = "/project/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    void deleteProject(@RequestHeader("cookie") String sessionId, @PathVariable("id") String id);

    @DeleteMapping(value = "/project/{id}", produces = MediaType.APPLICATION_XML_VALUE)
    void deleteProjectXml(@RequestHeader("cookie") String sessionId, @PathVariable("id") String id);

    @PostMapping(value = "/project", produces = MediaType.APPLICATION_JSON_VALUE)
    Project addProject(@RequestHeader("cookie") String sessionId, Project project);

    @PostMapping(value = "/project", produces = MediaType.APPLICATION_XML_VALUE)
    Project addProjectXml(@RequestHeader("cookie") String sessionId, Project project);

}
