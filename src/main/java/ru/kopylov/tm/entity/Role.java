package ru.kopylov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.kopylov.tm.enumerated.RoleType;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Getter
@Setter
@XmlRootElement
@NoArgsConstructor
@Table(name = "app_role")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Role extends AbstractEntity {

    @ManyToOne
    private User user;

    @Column
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

}
