<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>PROJECTS</title>
</head>
<body>
<h2><a href="/index.jsp">MAIN</a><jsp:text> | </jsp:text><a href="/project_list">PROJECTS</a><jsp:text> | </jsp:text><a href="/task_list">TASKS</a></h2>
<div align="center">
    <h2>PROJECT VIEW</h2>
    <table border="1" cellpadding="5">
        <tr>
            <th colspan="2">PROJECT</th>
        </tr>
        <tr>
            <th>ID</th>
            <th>${project.id}</th>
        </tr>
        <tr>
            <th>Name</th>
            <th>${project.name}</th>
        </tr>
        <tr>
            <th>Description</th>
            <th>${project.description}</th>
        </tr>
        <tr>
            <th>Date start</th>
            <th>${project.dateStart}</th>
        </tr>
        <tr>
            <th>Date finish</th>
            <th>${project.dateFinish}</th>
        </tr>
        <tr>
            <th>State</th>
            <th>${project.state}</th>
        </tr>
        <tr>
            <th colspan="2">
<%--                <a href="/project_edit/${project.id}">Edit</a>--%>
                <button><a href="/project_edit/${project.id}">Edit</a></button>
            </th>
        </tr>
    </table>
</div>
</body>
</html>
