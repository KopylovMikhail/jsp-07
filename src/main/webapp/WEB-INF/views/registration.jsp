<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Registration</title>
</head>
<body>
<h2><a href="/index.jsp">MAIN</a><jsp:text> | </jsp:text><a href="/project_list">PROJECTS</a>
    <jsp:text> | </jsp:text><a href="/task_list">TASKS</a><jsp:text> | </jsp:text><a href="/logout">LOGOUT</a></h2>

<div align="center">
<h4>Registration Form</h4>
<form action='<spring:url value="/registration"/>' method="post">
    <table>
        <tr>
            <td>Username</td>
            <td><input type="text" name="username"></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name="password"></td>
        </tr>
        <tr>
            <td><button type="submit">Registration</button></td>
        </tr>
    </table>
</form>
</div>
<br/>
</body>
</html>