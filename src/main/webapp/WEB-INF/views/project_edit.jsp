<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <%--    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>--%>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>PROJECTS</title>
</head>
<body>
<h2><a href="/index.jsp">MAIN</a><jsp:text> | </jsp:text><a href="/project_list">PROJECTS</a><jsp:text> | </jsp:text><a href="/task_list">TASKS</a></h2>
<div align="center">
    <h2>PROJECT EDIT</h2>
    <form:form action="edit" method="post">
        <table border="1" cellpadding="5">
            <tr>
                <th colspan="2">PROJECT</th>
            </tr>
            <tr>
                <th>ID</th>
                <th>
                    ${project.id}
                    <input type="hidden" name="id" placeholder="${project.id}" readonly="readonly" value="${project.id}"/>
                </th>
            </tr>
            <tr>
                <th>Name</th>
                <th>
                    <input type="text" name="name" value="${project.name}"/>
                </th>
            </tr>
            <tr>
                <th>Description</th>
                <th>
                    <input type="text" name="description" value="${project.description}"/>
                </th>
            </tr>
            <tr>
                <th>Date Start</th>
                <th>
                    <input type="datetime" name="dateStart" value="${project.dateStart}"
                           pattern="[0-9]{4}-\d\d-\d\d\s\d\d:\d\d:\d\d.[0-9]{,3}" placeholder="yyyy-MM-dd HH:mm:ss.S"/>
                </th>
            </tr>
            <tr>
                <th>Date Finish</th>
                <th>
                    <input type="datetime" name="dateFinish" value="${project.dateFinish}"
                           pattern="[0-9]{4}-\d\d-\d\d\s\d\d:\d\d:\d\d.[0-9]{,3}" placeholder="yyyy-MM-dd HH:mm:ss.S"/>
                </th>
            </tr>
            <tr>
                <th>State</th>
                <th>
<%--                    <input type="text" list="state-list" name="state" value="${project.state}"/>--%>
                    <input type="text" list="state-list" name="state" placeholder="Choose state" required/>
                    <datalist id="state-list">
                        <option value="PLANNED">
                        <option value="PROCESS">
                        <option value="DONE">
                    </datalist>
                </th>
            </tr>
            <tr>
                <th colspan="2"><button type="submit">Save</button></th>
            </tr>
        </table>
    </form:form>
</div>
</body>
</html>
