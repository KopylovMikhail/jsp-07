https://gitlab.com/KopylovMikhail/jsp-07

<b>SOFTWARE REQUIREMENTS:</b>
 - JRE 1.8
 
<b>TECHNOLOGY STACK:</b>
  - IntelliJ IDEA,
  - JAVA 8,
  - Maven 4.0,
  - Spring Boot 2.3.1
  
<b>DEVELOPER:</b>

 Mikhail Kopylov
<br> e-mail: kopylov_m88@mail.ru

<b>BUILD COMMANDS:</b>
```
mvn clean
```
```
mvn install
```
```
mvn tomcat7:run
```